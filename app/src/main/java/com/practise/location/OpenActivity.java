package com.practise.location;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class OpenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open);
    }

    public  void gotoLocation(View view)
    {
        Intent myIntent = new Intent(OpenActivity.this,MapsActivity.class);
        startActivity(myIntent);
    }

    public  void getUpdate(View view)
    {
        Intent intent = new Intent(OpenActivity.this,MainActivity.class);
        startActivity(intent);
    }
}
